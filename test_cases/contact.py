import requests
import unittest
import logging
import sys

from test_cases.helper.settings import *
from test_cases.helper.utility import *

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(formatter)

fileHandler = logging.FileHandler("contact.log")
fileHandler.setFormatter(formatter)
root.addHandler(fileHandler)


def create_contact(url, headers, data):
    response = requests.post(url, json=data, headers=headers)
    return response


class RestApiTest(unittest.TestCase):

    def setUp(self):
        # define Api URL and API Key
        self.api_url = "{0}/system/netsuite/api/contacts".format(BASE_URL)
        self.first_name = get_random_string()
        self.last_name = get_random_string()
        self.email = get_random_email()
        self.headers = {'content-type': 'application/json',
                   'Authorization': 'Basic YXBpOmFkbWlu'}
        self.data = {
            "firstName": self.first_name,
            "lastName": self.last_name,
            "email": self.email,
            "workPhone": "215-455-56555",
            "mobilePhone": "715-455-5575",
            "customerId": "3664"
        }

    def test_create_contact(self):
        # define api response

        logging.info("Sending data {}".format(self.data))

        resp = create_contact(url=self.api_url, data=self.data, headers=self.headers)

        logging.info("Got response: {}".format(resp.status_code))

        if resp.status_code != 200:
            logging.info('{0}'.format(resp.json()))
        else:
            logging.info('{0}'.format(resp.json()))

        self.assertEqual(200, resp.status_code)


    def test_update_contact(self):
        # define api response

        resp = create_contact(self.api_url, self.data, self.headers)
        logging.info("Sending data {}".format(self.data))

        update_data = {
            "inactive": False,
            "portalContactId": "1236789"
        }

        if resp.status_code != 200:
            logging.info('{0}'.format(resp.json()))
            update_resp = resp.status_code
        else:
            url = "{0}/{1}".format(self.api_url, resp.json()['id'])
            update_resp = requests.patch(url=url, data=update_data)
            logging.info('{0}'.format(update_resp.json()))

        self.assertEqual(200, update_resp.status_code)


    def test_delete_contact(self):
        # define api response

        logging.info("Sending data {}".format(self.data))
        resp = create_contact(self.api_url, self.data, self.headers)

        if resp.status_code != 202:
            logging.info('{0}'.format(resp.json()))
            delete_resp = resp.status_code
        else:
            url =  "{0}/{1}".format(self.api_url, resp.json()['id'])
            delete_resp = requests.delete(url)
            logging.info('{0}'.format(delete_resp.json()))

        self.assertEqual(202, delete_resp.status_code)


    def test_get_contact(self):
        # define api response

        resp = create_contact(self.api_url, self.data, self.headers)
        logging.info("Sending data {}".format(self.data))

        if resp.status_code != 200:
            logging.info('{0}'.format(resp.json()))
            get_resp = resp.status_code
        else:
            url = "{0}/{1}".format(self.api_url, resp.json()['id'])
            get_resp = requests.get(url=url)
            logging.info('{0}'.format(get_resp.json()))

        self.assertEqual(200, get_resp.status_code)

    def tearDown(self):
        print("test over")


if __name__ == "__main__":
    unittest.main()

