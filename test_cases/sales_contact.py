import requests
import unittest
import logging
import sys

from test_cases.helper.settings import *
from test_cases.helper.utility import *

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(formatter)

fileHandler = logging.FileHandler("contact.log")
fileHandler.setFormatter(formatter)
root.addHandler(fileHandler)


def create_contact(url, headers, data):
    response = requests.post(url, json=data, headers=headers)
    return response


class SalesContactApiTest(unittest.TestCase):

    def setUp(self):
        # define Api URL and API Key
        self.api_url = "{0}/system/salesforce/api/contacts".format(BASE_URL)
        self.first_name = get_random_string()
        self.last_name = get_random_string()
        self.email = get_random_email()
        self.headers = {'content-type': 'application/json',
                   'Authorization': 'Basic YXBpOmFkbWlu'}
        self.data = [{
              "firstName":get_random_string(),
              "lastName": get_random_string(),
              "workPhone": "213-450-5555",
              "mobilePhone": "711-545-5535",
              "email": get_random_email(),
              "title": "King",
              "portalContactId": random_with_N_digits(5),
              "accountId": "0011k00000CgIPGAA3"
            }]

    def test_create_contact(self):
        # define api response

        logging.info("Sending data {}".format(self.data))

        resp = create_contact(url=self.api_url, data=self.data, headers=self.headers)

        logging.info("Got response: {}".format(resp.status_code))

        if resp.status_code != 200:
            logging.info('{0}'.format(resp.json()))
        else:
            logging.info('{0}'.format(resp.json()))

        self.assertEqual(200, resp.status_code)


    def test_update_contact(self):
        # define api response

        resp = create_contact(self.api_url, self.data, self.headers)
        logging.info("Sending data {}".format(self.data))

        update_data = {
              "netsuiteCustomerId": "3664",
              "firstName": get_random_string(),
              "lastName": get_random_string(),
              "workPhone": "212-555-55555",
              "mobilePhone": "713-555-5555",
              "id": "0031k00000NEp8UAAT",
              "portalContactId": "428ec540-f56c-43c0-a85c-0a23949-test",
              "active": True
            }

        if resp.status_code != 200:
            logging.info('{0}'.format(resp.json()))
            update_resp = resp.status_code
        else:
            url = "{0}/{1}".format(self.api_url, resp.json()['portalContactId'])
            update_resp = requests.patch(url=url, data=update_data)
            logging.info('{0}'.format(update_resp.json()))

        self.assertEqual(200, update_resp.status_code)


    def tearDown(self):
        print("test over")


if __name__ == "__main__":
    unittest.main()

