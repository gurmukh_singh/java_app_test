import random
import string
from random import randint

def get_random_number(start=10, end=30):
    return random.choice(range(start, end))


def random_with_N_digits(n):
    """
    Generates an arbitrary number of digits of a given length.
    """

    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1

    return randint(range_start, range_end)

def get_random_string(length=get_random_number()):
    letters = string.ascii_lowercase + string.ascii_uppercase
    return ''.join(random.choice(letters) for i in range(length))


def get_random_domain(domains):
    return random.choice(domains)


def get_random_name(letters):
    return ''.join(random.choice(letters) for i in range(7))


def generate_random_emails():
    domains = ["hotmail.com", "gmail.com", "aol.com", "mail.com", "mail.kz", "yahoo.com"]
    return [get_random_string() + '@' + get_random_domain(domains) for i in range(10)]


def get_random_email():
    email = generate_random_emails()
    return email[0]

def random_phone():
    p=list('0000000000')
    p[0] = str(random.randint(1,9))
    for i in [1,2,6,7,8]:
        p[i] = str(random.randint(0,9))
    for i in [3,4]:
        p[i] = str(random.randint(0,8))
    if p[3]==p[4]==0:
        p[5]=str(random.randint(1,8))
    else:
        p[5]=str(random.randint(0,8))
    n = range(10)
    if p[6]==p[7]==p[8]:
        n = (i for i in n if i!=p[6])
    p[9] = str(random.choice(n))
    p = ''.join(p)
    return p[:3] + '-' + p[3:6] + '-' + p[6:]    
    
    
    
