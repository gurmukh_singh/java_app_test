import requests
import unittest
import logging
import sys

from test_cases.helper.settings import *
from test_cases.helper.utility import *

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(formatter)

fileHandler = logging.FileHandler("contact.log")
fileHandler.setFormatter(formatter)
root.addHandler(fileHandler)


def create_customer(url, headers, data):
    response = requests.post(url, json=data, headers=headers)
    return response


class CustomerApiTest(unittest.TestCase):

    def setUp(self):
        # define Api URL and API Key
        self.api_url = "{0}/system/netsuite/api/customers".format(BASE_URL)
        self.headers = {'content-type': 'application/json',
                   'Authorization': 'Basic YXBpOmFkbWlu'}
        self.data = {
              "address": [
                {
                  "address1": "10927 Southwest 44th Street",
                  "city": "Mustang",
                  "country": "Japan",
                  "postalCode": "73064",
                  "stateProvince": "OK",
                  "isDefaultBilling": True,
                  "isDefaultShipping": True
                }
              ],
              "companyName": "rene ",
              "companyPhone": "4057789768",
              "alternatePhone": "4057789768",
              "email": get_random_email(),
              "salutation": "Dr.",
              "firstName": get_random_string() ,
              "lastName": get_random_string(),
              "contactName": "re ne",
              "sfdcCustomerId": "0011k00000F7c3ZAAR",
              "salesRep": "Rene Lucena",
              "currency": "USD",
              "sourceSystem": "SALESFORCE",
              "recordTypeId": "0121k0000008XXKAA2",
              "recordTypeName": "Channel Prospect",
              "type": "Prospect",
              "contact": {
                "roleId": "1046",
                "email": get_random_email(),
                "firstName": get_random_string(),
                "lastName": get_random_string(),
                "mobilePhone": "4057789769",
                "workPhone": "4057789769"
              }
            }


    def test_create_customer(self):
        # define api response

        logging.info("Sending data {}".format(self.data))

        resp = create_customer(url=self.api_url, data=self.data, headers=self.headers)

        logging.info("Got response: {}".format(resp.status_code))

        if resp.status_code != 200:
            logging.info('{0}'.format(resp.json()))
        else:
            logging.info('{0}'.format(resp.json()))

        self.assertEqual(200, resp.status_code)

    def test_get_customer(self):
        # define api response

        resp = create_customer(self.api_url, self.data, self.headers)
        logging.info("Sending data {}".format(self.data))

        if resp.status_code != 200:
            logging.info('{0}'.format(resp.json()))
            get_resp = resp.status_code
        else:
            url = "{0}/{1}".format(self.api_url, resp.json()['id'])
            get_resp = requests.get(url=url)
            logging.info('{0}'.format(get_resp.json()))

        self.assertEqual(200, get_resp.status_code)

    def tearDown(self):
        print("test over")


if __name__ == "__main__":
    unittest.main()

