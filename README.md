# Java App Test Cases
Test Cases

# Commands to run test case for Contact
- python -m unittest test_cases.contact.RestApiTest.test_create_contact

- python -m unittest test_cases.contact.RestApiTest.test_update_contact

- python -m unittest test_cases.contact.RestApiTest.test_delete_contact

- python -m unittest test_cases.contact.RestApiTest.test_get_contact